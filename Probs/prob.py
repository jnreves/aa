import numpy as np

def normal_eq(x, y):
    #beta = np.dot(np.dot(np.linalg.inv(np.dot(np.transpose(x), x)), np.transpose(x)), y)
    beta = np.matmul(np.matmul(np.linalg.inv(np.matmul(np.transpose(x), x)), np.transpose(x)), y)
    return beta


x = np.array([[1, 24], [1, 30], [1, 36]])
y = np.array([[13], [14], [16]])
beta = normal_eq(x, y)
print(beta)

def calculate_error(x, y):
    error = np.square(np.linalg.norm(y - np.matmul(x, beta)))
    return error

error = calculate_error(x,y)
print(error)
test = np.array([[24], [25]])
y = np.zeros(2)
y.shape = (2,1)
y = beta[1] *test + beta[0]
print(y)
    
