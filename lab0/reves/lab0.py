import numpy as np
import matplotlib.pyplot as plt
a = np.array([1, 2, 3, 4, 5])
print(a)
M = np.array([[1, 2], [3, 4], [5, 6]])
print(M)
z = np.zeros(5)
print(z)
Z = np.array([np.ones(3), np.zeros(3)])
print(Z)
I = np.identity(4)
print(I)
B = np.linalg.inv(I)
print(B)
b = a +3
print(b)
det = np.linalg.det(I)
print(det)
mi= np.min(a)
print(mi)
ma= np.max(a)
print(ma)

np.save('matrix.npy', M+4)
x = np.load('matrix.npy')
print(x)

x = np.random.normal(5, 0.1, 100)

plt.plot(x)
