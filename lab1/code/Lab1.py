import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge, Lasso, LinearRegression


def fit_polynomial(degree, x, y):
    M = np.zeros((degree + 1, degree + 1))
    A = np.zeros(degree + 1)
    A.shape = (degree + 1, 1)
    for i in range(degree + 1):
        _sum_y = 0
        for j in range(degree + 1):
            _sum_x = 0
            for a in x:
                _sum_x = _sum_x + pow(a,i+j)
            M[i,j] = _sum_x
    
        for l in range(len(y)):
            _sum_y = _sum_y + y[l]*pow(x[l],i)
        A[i] = _sum_y
    B = np.matmul(np.linalg.inv(M), A)
    return B


def calculate_SSE(y, y_exp):
    error = 0
    error = np.square(np.linalg.norm(y - y_exp))
    return error
    
x = np.load('data1_x.npy')
y = np.load('data1_y.npy')


#2.1.3
Beta = fit_polynomial(1,x,y)
y_exp =  Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(1)
plt.scatter(x,y, label = 'actual data')
plt.plot(x, y_exp, color = 'red', label = 'estimated solution')
plt.legend(loc='upper left')
plt.xlabel('x')
plt.ylabel('y')




#2.1.4
x = np.load('data2_x.npy')
y = np.load('data2_y.npy')
Beta = fit_polynomial(2,x,y)
y_exp = Beta[2] * pow(x,2) + Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_2 = ", Beta[2], "Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(2)
plt.scatter(x,y, label = 'actual data')
plt.scatter(x, y_exp, color = 'red', label = 'estimated solution')
plt.legend(loc='upper left')
plt.xlabel('x')
plt.ylabel('y')


#2.1.5
x = np.load('data2a_x.npy')
y = np.load('data2a_y.npy')
Beta = fit_polynomial(2,x,y)
y_exp = Beta[2] * pow(x,2) + Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_2 = ", Beta[2], "Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(3)
plt.scatter(x,y, label = 'actual data')
plt.scatter(x, y_exp, color = 'red', label = 'estimated solution')
plt.legend(loc='upper left')
plt.xlabel('x')
plt.ylabel('y')

def remove_outliers(y, y_exp, x):
    new_y = []
    new_x = []
    for i in range(len(y)):
        error = np.abs(y_exp[i] - y[i])
        if error > 1:
            new_y = np.delete(y,i)
            new_x = np.delete(x, i)
    
    new_y = new_y.reshape(len(new_y),1)
    new_x = new_x.reshape(len(new_x),1)
    return new_y, new_x
                  
y, x = remove_outliers(y,y_exp,x)
Beta = fit_polynomial(2,x,y)
y_exp = Beta[2] * pow(x,2) + Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_2 = ", Beta[2], "Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(4)
plt.scatter(x,y, label = 'actual data')
plt.scatter(x, y_exp, color = 'red', label = 'estimated solution')
plt.legend(loc='upper left')
plt.xlabel('x')
plt.ylabel('y')

#Part2



#2.2.1
x = np.load('data3_x.npy')
y = np.load('data3_y.npy')
#2.2.3
ridge = Ridge(max_iter = 10000, copy_X = True)
lasso = Lasso(max_iter = 10000, copy_X = True)
reg = LinearRegression(copy_X = True)


#2.2.4
coefs_ridge = []
coefs_lasso = []
coefs_reg = []

reg.fit(x,y)

y.shape = (len(y),)


alphas = np.arange(pow(10,-3),10, 0.01)
for a in alphas:
    ridge.set_params(alpha = a)
    ridge.fit(x, y)
    coefs_ridge.append(ridge.coef_)
    lasso.set_params(alpha = a)
    lasso.fit(x, y)
    coefs_lasso.append(lasso.coef_)

coefs_reg = np.full((len(alphas),3), reg.coef_)
coefs_ridge = np.asarray(coefs_ridge)  
coefs_ridge = np.concatenate((coefs_ridge, coefs_reg), axis = 1)
coefs_lasso = np.asarray(coefs_lasso)  
coefs_lasso = np.concatenate((coefs_lasso, coefs_reg), axis = 1)

#2.2.5
plt.figure(5)
ax = plt.gca()
ax.plot(alphas, coefs_ridge[:,0], label='beta[0]')
ax.plot(alphas, coefs_ridge[:,1], label='beta[1]')
ax.plot(alphas, coefs_ridge[:,2], label='beta[2]')
ax.plot(alphas, coefs_ridge[:,3], label='LS_beta[0]')
ax.plot(alphas, coefs_ridge[:,4], label='LS_beta[1]')
ax.plot(alphas, coefs_ridge[:,5], label='LS_beta[2]')
ax.legend(loc='upper left')
ax.set_xscale('log')
plt.axis('tight')
plt.xlabel('alpha')
plt.ylabel('weights')

np.shape(coefs_lasso)
plt.figure(6)
ax = plt.gca()
ax.plot(alphas, coefs_lasso[:,0], label='beta[0]')
ax.plot(alphas, coefs_lasso[:,1], label='beta[1]')
ax.plot(alphas, coefs_lasso[:,2], label='beta[2]')
ax.plot(alphas, coefs_lasso[:,3], label='LS_beta[0]')
ax.plot(alphas, coefs_lasso[:,4], label='LS_beta[1]')
ax.plot(alphas, coefs_lasso[:,5], label='LS_beta[2]')
ax.legend(loc='upper left')
ax.set_xscale('log')
plt.axis('tight')
plt.xlabel('alpha')
plt.ylabel('weights')



#2.2.7
a = 0.08
lasso.set_params(alpha = a)
reg.fit(x,y)
lasso.fit(x,y)
reg.coef_.shape = (3,)
print(lasso.coef_)
b = lasso.coef_
print(reg.coef_)
y_reg = np.zeros((len(x),1))
y_lasso = np.zeros((len(x),1))
for i in range(len(x)):
    y_lasso[i] = x[i,0] * lasso.coef_[0] + x[i,1] * lasso.coef_[1] + x[i,2] * lasso.coef_[2] + lasso.intercept_
    y_reg[i] = x[i,0] * reg.coef_[0] + x[i,1] * reg.coef_[1] + x[i,2] * reg.coef_[2] + reg.intercept_

plt.figure(7)
plt.plot(y, color = 'green', label='actual data')
plt.plot(y_reg, color = 'blue', label='LS')
plt.plot(y_lasso, color = 'red', label='Lasso')
plt.legend(loc='upper right')
reg_SSE = calculate_SSE(y, y_reg)
lasso_SSE = calculate_SSE(y, y_lasso)
print(reg_SSE)
print(lasso_SSE)


