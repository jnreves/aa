import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def fit_polynomial(degree, x, y):
    M = np.zeros((degree + 1, degree + 1))
    A = np.zeros(degree + 1)
    A.shape = (degree + 1, 1)
    for i in range(degree + 1):
        _sum_y = 0
        for j in range(degree + 1):
            _sum_x = 0
            for a in x:
                _sum_x = _sum_x + pow(a,i+j)
            M[i,j] = _sum_x
    
        for l in range(len(y)):
            _sum_y = _sum_y + y[l]*pow(x[l],i)
        A[i] = _sum_y
    B = np.matmul(np.linalg.inv(M), A)
    return B


def calculate_SSE(y, y_exp):
    error = np.square(np.linalg.norm(y - y_exp))
    return error
    
x = np.load('data1_x.npy')
y = np.load('data1_y.npy')


Beta = fit_polynomial(1,x,y)
y_exp =  Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(1)
plt.scatter(x,y)
plt.plot(x, y_exp, color = 'red')


x = np.load('data2_x.npy')
y = np.load('data2_y.npy')
Beta = fit_polynomial(2,x,y)
y_exp = Beta[2] * pow(x,2) + Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_2 = ", Beta[2], "Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(2)
plt.scatter(x,y)
plt.scatter(x, y_exp, color = 'red')

x = np.load('data2a_x.npy')
y = np.load('data2a_y.npy')
Beta = fit_polynomial(2,x,y)
y_exp = Beta[2] * pow(x,2) + Beta[1]*x + Beta[0]
SSE = calculate_SSE(y, y_exp)
print("Beta_2 = ", Beta[2], "Beta_1 = " ,Beta[1], " Beta_0 = ", Beta[0])
print("SSE = ", SSE)
plt.figure(3)
plt.scatter(x,y)
plt.scatter(x, y_exp, color = 'red')



