import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression

x = np.load('data3_x.npy')
y = np.load('data3_y.npy')
#print(x)
#print(y)
#plt.figure(4)
#plt.show()

rig = Ridge(alpha=1.0)
rig.fit(x, y)

Ridge(alpha=0.5, copy_X=True, fit_intercept=True, max_iter=None,
      normalize=False, random_state=None, solver='auto', tol=0.001)
print("Ridge")
print(rig.coef_)



las = Lasso(alpha=0.1)
las.fit(x, y)

Lasso(alpha=0.1, copy_X=True, fit_intercept=True, max_iter=1000,
   normalize=False, positive=False, precompute=False, random_state=None,
   selection='cyclic', tol=0.0001, warm_start=False)
print("Lasso")
print(las.coef_)


lin = LinearRegression()

lin.fit(x, y)

LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None,
                 normalize=False)
print("Linear Regression")
print(lin.coef_)
