|                             | Number of tests | eta      | alpha | iterations |
|-----------------------------|-----------------|----------|-------|------------|
| Without adaptive step sizes | -               | 0.012402 | 0.889 | 468        |
| Without adaptive step sizes | *               | 0.01378  | 0.889 | 19         |
| Without adaptive step sizes | -               | 0.015158 | 0.889 | >1000      |
| With adaptive step sizes    | -               | 0.013797 | 0.889 | 362        |
| With adaptive step sizes    | *               | 0.01533  | 0.889 | 21         |
| With adaptive step sizes    | -               | 0.016863 | 0.889 | 365        |
