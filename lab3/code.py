import numpy as np
import keras as ks
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix


x_train = np.load('data_lab3/mnist_train_data.npy')
x_test = np.load('data_lab3/mnist_test_data.npy')
y_train = np.load('data_lab3/mnist_train_labels.npy')
y_test = np.load('data_lab3/mnist_test_labels.npy')

x_train = x_train/255
x_test = x_test/255

y_train = ks.utils.to_categorical(y_train, num_classes = 10)
y_test = ks.utils.to_categorical(y_test, num_classes = 10)

#plt.imshow(x_train[100]) 

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size = 0.3)

#------------------------------MLP----------------------------#

model = ks.models.Sequential()
model.add(ks.layers.Flatten(input_shape=(28, 28, 1)))
model.add(ks.layers.Dense(64, activation = 'relu'))
model.add(ks.layers.Dense(128, activation = 'relu'))
model.add(ks.layers.Dense(10, activation = 'softmax'))

model_withES = ks.models.Sequential()
model_withES.add(ks.layers.Flatten(input_shape=(28, 28, 1)))
model_withES.add(ks.layers.Dense(64, activation = 'relu'))
model_withES.add(ks.layers.Dense(128, activation = 'relu'))
model_withES.add(ks.layers.Dense(10, activation = 'softmax'))

print(model.summary())

optimizer = ks.optimizers.Adam(learning_rate = 0.01, clipnorm = 1)
monitor = ks.callbacks.EarlyStopping(patience = 15, restore_best_weights = True)

model.compile(loss = 'categorical_crossentropy', optimizer = optimizer, metrics = ['accuracy'])
model_withES.compile(loss = 'categorical_crossentropy', optimizer = optimizer, metrics = ['accuracy'])
history_withES = model_withES.fit(x_train, y_train, validation_data=(x_validation, y_validation), batch_size = 300, callbacks = [monitor], epochs = 400)
history = model.fit(x_train, y_train, validation_data=(x_validation, y_validation), batch_size = 300, epochs = 400)


plt.figure(1)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.title('Model Loss without Early Stopping')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

plt.figure(2)
plt.plot(history_withES.history['loss'])
plt.plot(history_withES.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.title('Model Loss with Early Stopping')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

#------------------------------CNN----------------------------#

cnn = ks.models.Sequential()
cnn.add(ks.layers.Conv2D(16, kernel_size = 3,activation = 'relu', input_shape=(28, 28, 1)))
cnn.add(ks.layers.MaxPooling2D(pool_size = (2,2)))
cnn.add(ks.layers.Conv2D(32, kernel_size = 3,activation = 'relu'))
cnn.add(ks.layers.MaxPooling2D(pool_size = (2,2)))
cnn.add(ks.layers.Flatten(input_shape=(5, 5, 32)))
cnn.add(ks.layers.Dense(64, activation = 'relu'))
cnn.add(ks.layers.Dense(10, activation = 'softmax'))
print(cnn.summary())

cnn.compile(loss = 'categorical_crossentropy', optimizer = optimizer, metrics = ['accuracy'])
history = cnn.fit(x_train, y_train, validation_data=(x_validation, y_validation), batch_size = 300, callbacks = [monitor], epochs = 400)

# Plot training & validation loss values
plt.figure(3)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.title('Model Loss with Early Stopping')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

print('Results:')
predictions = model.predict([x_test])
acc = accuracy_score(y_test, predictions.round())
print('MLP accuracy without Early Stopping: {}'.format(acc))
matrix = confusion_matrix(y_test.argmax(axis=1), predictions.argmax(axis=1))
print('MLP Confusion Matrix without Early Stopping: {}'.format(matrix))


predictions = model_withES.predict([x_test])
acc = accuracy_score(y_test, predictions.round())
print('MLP accuracy with Early Stopping: {}'.format(acc))
matrix = confusion_matrix(y_test.argmax(axis=1), predictions.argmax(axis=1))
print('MLP Confusion Matrix with Early Stopping: {}'.format(matrix))


predictions = cnn.predict([x_test])
acc = accuracy_score(y_test, predictions.round())
print('CNN accuracy with Early Stopping: {}'.format(acc))
matrix = confusion_matrix(y_test.argmax(axis=1), predictions.argmax(axis=1))
print('CNN Confusion Matrix with Early Stopping: {}'.format(matrix))
