import numpy as np
import matplotlib.pylab as plot
import matplotlib.image as mpimg

from keras import to_categorical


train_data = np.load('mnist_train_data.npy')
train_label = np.load('mnist_train_labels.npy')
test_data = np.load('mnist_test_data.npy')
test_data = np.load('mnist_test_labels.npy')

print('Sizeof train_data')
print(np.shape(train_data))

print('Sizeof train_label')
print(np.shape(train_label))

train_data = np.true_divide(train_data, 255)
test_data = np.true_divide(test_data, 255)

train_label = to_categorical(train_data)
