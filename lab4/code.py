import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import CountVectorizer
import sklearn.metrics as met
from math import sqrt
from math import exp
from math import pi

#----------------------------2---------------------------#
def str_column_to_float(dataset, column):
	for row in dataset:
		row[column] = float(row[column].strip())

def str_column_to_int(dataset, column):
	class_values = [row[column] for row in dataset]
	unique = set(class_values)
	lookup = dict()
	for i, value in enumerate(unique):
		lookup[value] = i
		print('[%s] => %d' % (value, i))
	for row in dataset:
		row[column] = lookup[row[column]]
	return lookup

def separate_by_class(dataset):
	separated = dict()
	for i in range(len(dataset)):
		vector = dataset[i]
		class_value = vector[-1]
		if (class_value not in separated):
			separated[class_value] = list()
		separated[class_value].append(vector)
	return separated

def mean(numbers):
	return sum(numbers)/float(len(numbers))

def stdev(numbers):
	avg = mean(numbers)
	variance = sum([(x-avg)**2 for x in numbers]) / float(len(numbers)-1)
	return sqrt(variance)

def summarize_dataset(dataset):
	summaries = [(mean(column), stdev(column), len(column)) for column in zip(*dataset)]
	del(summaries[-1])
	return summaries

def summarize_by_class(dataset):
	separated = separate_by_class(dataset)
	summaries = dict()
	for class_value, rows in separated.items():
		summaries[class_value] = summarize_dataset(rows)
	return summaries

def calculate_probability(x, mean, stdev):
	exponent = exp(-((x-mean)**2 / (2 * stdev**2 )))
	return (1 / (sqrt(2 * pi) * stdev)) * exponent

def calculate_class_probabilities(summaries, row):
	total_rows = sum([summaries[label][0][2] for label in summaries])
	probabilities = dict()
	for class_value, class_summaries in summaries.items():
		probabilities[class_value] = summaries[class_value][0][2]/float(total_rows)
		for i in range(len(class_summaries)):
			mean, stdev, _ = class_summaries[i]
			probabilities[class_value] *= calculate_probability(row[i], mean, stdev)
	return probabilities

def predict(summaries, row):
	probabilities = calculate_class_probabilities(summaries, row)
	best_label, best_prob = None, -1
	for class_value, probability in probabilities.items():
		if best_label is None or probability > best_prob:
			best_prob = probability
			best_label = class_value
	return best_label




xtrain = np.load('data1_xtrain.npy')
ytrain = np.load('data1_ytrain.npy')
xtest = np.load('data1_xtest.npy')
ytest = np.load('data1_ytest.npy')
dataset = np.concatenate((xtrain, ytrain),axis=1)

categories = dataset[:,2]
categories = categories.astype(int)
colormap = np.array(['r', 'g', 'b'])

c1 = mpatches.Patch(color='r', label='Class 1')
c2 = mpatches.Patch(color='g', label='Class 2')
c3 = mpatches.Patch(color='b', label='Class 3')
                       
                       
plt.figure(1)
plt.scatter(dataset[:,0], dataset[:,1], c = colormap[categories-1])
plt.xlim([-5, 7.5])
plt.ylim([-5, 7.5])

plt.legend(handles=[c1,c2, c3])
 
model = summarize_by_class(dataset)

label = np.zeros(len(ytest))
i = 0
for x in xtest:
    label[i] = predict(model, x)
    i = i +1

accuracy = met.accuracy_score(ytest, label)
print("The accuracy of the simple example:", accuracy)



#----------------------------3---------------------------#



pt = pd.read_csv('pt_trigram_count.tsv', sep = '\t')
es = pd.read_csv('es_trigram_count.tsv', sep = '\t')
fr = pd.read_csv('fr_trigram_count.tsv', sep = '\t')
en = pd.read_csv('en_trigram_count.tsv', sep = '\t')

pt = pd.DataFrame.to_numpy(pt)
es = pd.DataFrame.to_numpy(es)
fr = pd.DataFrame.to_numpy(fr)
en = pd.DataFrame.to_numpy(en)


dataset = np.concatenate((pt,es,fr,en), axis = 1)

X_train = np.transpose(dataset[:,[2,5,8,11]])
#y_train = np.array([[0], [1], [2], [3]])
y_train = np.array([0,1,2,3])


model = MultinomialNB(fit_prior = False)

model.fit(X_train, y_train)
predictions = model.predict(X_train)
acc = accuracy_score(y_train, predictions)
print("Recognizer accuracy: ", acc)

sentence_matrix = ['El cine está abierto.', 
                   'Tu vais à escola hoje.', 
                   'Tu vais à escola hoje pois já estás melhor.', 
                   'English is easy to learn.', 
                   'Tu vas au cinema demain matin.', 
                   'É fácil de entender.']


vectorizer = CountVectorizer(vocabulary = dataset[:,1], strip_accents = None, analyzer = 'char', ngram_range = (3,3))
X_test = vectorizer.fit_transform(sentence_matrix)
y_test = np.array([1,0,0,3,2,0])

predictions = model.predict(X_test)

acc = accuracy_score(y_test, predictions)
print("Test accuracy: ", acc)
score = model.predict_proba(X_test)
#print(score)
class_margin = []
for i in range(len(score)):
    list_ = sorted(score[i,:].tolist(), reverse = True)
    class_margin.append(list_[0] - list_[1])
    
plt.show()    
