import numpy as np;
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from svm_plot import plot_contours;
import math


#--------------------------1------------------------#
spiral_X = np.load('spiral_X.npy')
spiral_Y = np.load('spiral_Y.npy')

'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000)
    model.fit(spiral_X, spiral_Y)
    predictions = model.predict(spiral_X)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(spiral_Y, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
   
    '''
#best degree: 9   
'''
gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma =gamma, max_iter = 1000000)
    model.fit(spiral_X, spiral_Y)
    predictions = model.predict(spiral_X)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(spiral_Y, predictions)
    plot_contours(model, spiral_X, spiral_Y)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''
'''
model = SVC(gamma = 0.01, max_iter = 1000000)
model.fit(spiral_X, spiral_Y)
predictions = model.predict(spiral_X)
s_vectors_nb = len(model.support_vectors_)
acc = accuracy_score(spiral_Y, predictions)
plot_contours(model, spiral_X, predictions)
'''
#--------------------------------------------------#


#--------------------------2------------------------#

chess33_X = np.load('chess33_X.npy')
chess33_Y = np.load('chess33_Y.npy')
'''
gamma_range = np.linspace(0.001,0.05,20)
for gamma in gamma_range:
    model2 = SVC(C = math.inf, gamma = gamma, max_iter = 1000000)
    model2.fit(chess33_X, chess33_Y)
    predictions = model2.predict(chess33_X)
    #plot_contours(model2, chess33_X, chess33_Y)
    s_vectors_nb = len(model2.support_vectors_)
    acc = accuracy_score(chess33_Y, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")
    
#best gamma: 0.01 (10 support vectors)
'''
model2 = SVC(C = math.inf, gamma = 0.01, max_iter = 1000000)
model2.fit(chess33_X, chess33_Y)

#--------------------------------------------------#
    
#-------------------------3-----------------------#
    
chess33n_X = np.load('chess33n_X.npy')
chess33n_Y = np.load('chess33n_Y.npy')

model2.fit(chess33n_X, chess33n_Y)
predictions = model2.predict(chess33_X)
s_vectors_nb = len(model2.support_vectors_)
acc = accuracy_score(chess33_Y, predictions)
print("Number of support vectors: ", s_vectors_nb)
print("Accuracy: ", acc)
print("")

C_range = np.logspace(-5, 3, 9)
for c in C_range:
   '''
    model3 = SVC(C = c, gamma = 0.01, max_iter = 1000000)
    model3.fit(chess33n_X, chess33n_Y)
    predictions = model3.predict(chess33_X)
    #plot_contours(model3, chess33n_X, chess33n_Y)
    s_vectors_nb = len(model3.support_vectors_)
    acc = accuracy_score(chess33_Y, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")
    '''

model3 = SVC(C = 0.1, gamma = 0.01, max_iter = 1000000)
model3.fit(chess33n_X, chess33n_Y)
predictions = model3.predict(chess33_X)
plot_contours(model3, chess33n_X, chess33n_Y)
