import numpy as np;
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix, roc_curve, f1_score

#dataset 1
x1_train = np.load('data_lab6/dataset1_xtrain.npy')
y1_train = np.load('data_lab6/dataset1_ytrain.npy')
x1_test = np.load('data_lab6/dataset1_xtest.npy')
y1_test = np.load('data_lab6/dataset1_ytest.npy')

#dataset 2
x2_train = np.load('data_lab6/dataset2_xtrain.npy')
y2_train = np.load('data_lab6/dataset2_ytrain.npy')
x2_test = np.load('data_lab6/dataset2_xtest.npy')
y2_test = np.load('data_lab6/dataset2_ytest.npy')

'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)

'''
#best degree -> 1 -> accuracy: 0.77 ->support vectors: 549

'''
gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma = gamma, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
 
'''
#best gamma -> 0.0001 -> accuracy: 0.69 ->support vectors: 695
    
'''
C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")
 '''   
#best case:  C = 1000, accuracy = 0.75, support vectors = 488
    
 
# Hunt for the best
# Dataset 1
'''
C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, kernel = 'poly', degree = 1, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)

    matrix = confusion_matrix(y1_test, predictions)

    print(matrix)
'''
# Dataset 2

'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''

#best degree -> 5 -> accuracy: 0.8586 ->support vectors: 73

'''
gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma = gamma, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''

#best gamma -> 1e-07 -> accuracy: 0.8788 ->support vectors: 171

'''
C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")
'''

#best case:  C = all, accuracy = 0.6869, support vectors = 231    



# Dataset 2 with balanced

'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''

#best degree -> 5 -> accuracy: 0.8586 ->support vectors: 73

'''
gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma = gamma, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''

#best gamma -> 1e-07 -> accuracy: 0.8900 ->support vectors: 181 ========= better


C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")


#best case:  C = all, accuracy = 0.6869, support vectors = 231    