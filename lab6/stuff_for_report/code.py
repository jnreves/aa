import numpy as np;
from sklearn.svm import SVC
from sklearn.metrics import balanced_accuracy_score, accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

#dataset 1
x1_train = np.load('data_lab6/dataset1_xtrain.npy')
y1_train = np.load('data_lab6/dataset1_ytrain.npy')
x1_test = np.load('data_lab6/dataset1_xtest.npy')
y1_test = np.load('data_lab6/dataset1_ytest.npy')

#dataset 2
x2_train = np.load('data_lab6/dataset2_xtrain.npy')
y2_train = np.load('data_lab6/dataset2_ytrain.npy')
x2_test = np.load('data_lab6/dataset2_xtest.npy')
y2_test = np.load('data_lab6/dataset2_ytest.npy')

#---------------------------Support Vector Machines-------------------------
print("")
print("Support Vector Machine Classifier")
print("")
print("Dataset 1:")


#Dataset 1
'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)

'''
    

C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000)
    model.fit(x1_train, y1_train)
    predictions = model.predict(x1_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y1_test, predictions)
    b_acc = balanced_accuracy_score(y1_test, predictions)
    matrix = confusion_matrix(y1_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")
    print("Balanced Accuracy: ", b_acc)
    print("")
    
    print("Confusion matrix: ")
    print(matrix)
    print("")
     
# Dataset 2
'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)

#best degree -> 5 -> accuracy: 0.8586 ->support vectors: 73

gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma = gamma, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)




C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000)
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("")

#best case:  C = all, accuracy = 0.6869, support vectors = 231    

'''

# Dataset 2 with balanced

'''
for i in range(1,12): 
    model = SVC(kernel = 'poly', degree = i, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    b_acc = balanced_accuracy_score(y2_test, predictions)
    #plot_contours(model, spiral_X, spiral_Y)
    print("Degree: ",i)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
'''

#best degree -> 5 -> accuracy: 0.8586 ->support vectors: 73

'''
gamma_range = np.logspace(-9,3,13)
for gamma in gamma_range:
    model = SVC(gamma = gamma, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    b_acc = balanced_accuracy_score(y2_test, predictions)
    print("Gamma: ", gamma)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("Balanced accuracy: ", b_acc)
    print("")
'''

#best gamma -> 1e-07 -> accuracy: 0.8900 ->support vectors: 181 ========= better

'''
C_range = np.logspace(-5, 3, 9)
for c in C_range:
    model = SVC(C = c, gamma = 0.0001, max_iter = 1000000, class_weight = 'balanced')
    model.fit(x2_train, y2_train)
    predictions = model.predict(x2_test)
    s_vectors_nb = len(model.support_vectors_)
    acc = accuracy_score(y2_test, predictions)
    b_acc = balanced_accuracy_score(y2_test, predictions)
    print("C: ", c)
    print("Number of support vectors: ", s_vectors_nb)
    print("Accuracy: ", acc)
    print("Balanced accuracy: ", b_acc)
    print("")
'''

#best case:  C = all, accuracy = 0.6869, support vectors = 231   



#Final models for SVM
print("Final")
#best model for dataset 1
model = SVC(kernel = "poly", degree = 1, max_iter = 1000000)
model.fit(x1_train, y1_train)
predictions = model.predict(x1_test)
s_vectors_nb = len(model.support_vectors_)
acc = accuracy_score(y1_test, predictions)
b_acc = balanced_accuracy_score(y1_test, predictions)
matrix = confusion_matrix(y1_test, predictions)
print("Number of support vectors: ", s_vectors_nb)
print("Accuracy: ", acc)
print("Balanced accuracy: ", b_acc)
print("")
print("Confusion matrix:")
print(matrix)
print("")
#best model for dataset 2
print("Dataset 2:")
model = SVC(gamma = 1e-07, max_iter = 1000000)
model.fit(x2_train, y2_train)
predictions = model.predict(x2_test)
s_vectors_nb = len(model.support_vectors_)
acc = accuracy_score(y2_test, predictions)
b_acc = balanced_accuracy_score(y2_test, predictions)
matrix = confusion_matrix(y2_test, predictions)
print("Number of support vectors: ", s_vectors_nb)
print("Accuracy: ", acc)
print("Balanced accuracy: ", b_acc)
print("")
print("Confusion matrix:")
print(matrix)
print("")


   
#---------------------------Decision trees-------------------------
print("Decision Tree Classifier")
print("")


#dataset1
print("Dataset 1:")
model1 = DecisionTreeClassifier(max_depth = 17)
model1 = model1.fit(x1_train, y1_train)
predictions = model1.predict(x1_test)
acc = accuracy_score(y1_test, predictions)
b_acc = balanced_accuracy_score(y1_test, predictions)
matrix = confusion_matrix(y1_test, predictions)
print("Accuracy: ", acc)
print("Balanced accuracy: ", b_acc)
print("Confusion matrix: ")
print(matrix)
print("Depth: ", model1.get_depth())
print("")


#dataset2
print("Dataset 2:")
model2 = DecisionTreeClassifier(max_depth = 10)
model2 = model2.fit(x2_train, y2_train)
predictions = model2.predict(x2_test)
acc = accuracy_score(y2_test, predictions)
b_acc = balanced_accuracy_score(y2_test, predictions)
matrix = confusion_matrix(y2_test, predictions)
print("Accuracy: ", acc)
print("Balanced accuracy: ", b_acc)
print("Confusion matrix: ")
print(matrix)
print("Depth: ", model2.get_depth())



#Code for pruning
'''
X_train, X_val, y_train, y_val = train_test_split(x1_train, y1_train, test_size = 0.3)
clf = DecisionTreeClassifier(random_state=0)
path = clf.cost_complexity_pruning_path(X_train, y_train)
ccp_alphas, impurities = path.ccp_alphas, path.impurities
fig, ax = plt.subplots()
ax.plot(ccp_alphas[:-1], impurities[:-1], marker='o', drawstyle="steps-post")
ax.set_xlabel("effective alpha")
ax.set_ylabel("total impurity of leaves")
ax.set_title("Total Impurity vs effective alpha for training set")




clfs = []
for ccp_alpha in ccp_alphas:
    clf = DecisionTreeClassifier(random_state=0, ccp_alpha=ccp_alpha)
    clf.fit(X_train, y_train)
    clfs.append(clf)
print("Number of nodes in the last tree is: {} with ccp_alpha: {}".format(
      clfs[-1].tree_.node_count, ccp_alphas[-1]))



clfs = clfs[:-1]
ccp_alphas = ccp_alphas[:-1]

node_counts = [clf.tree_.node_count for clf in clfs]
depth = [clf.tree_.max_depth for clf in clfs]
fig, ax = plt.subplots(2, 1)
ax[0].plot(ccp_alphas, node_counts, marker='o', drawstyle="steps-post")
ax[0].set_xlabel("alpha")
ax[0].set_ylabel("number of nodes")
ax[0].set_title("Number of nodes vs alpha")
ax[1].plot(ccp_alphas, depth, marker='o', drawstyle="steps-post")
ax[1].set_xlabel("alpha")
ax[1].set_ylabel("depth of tree")
ax[1].set_title("Depth vs alpha")
fig.tight_layout()


train_scores = [clf.score(X_train, y_train) for clf in clfs]
validation_scores = [clf.score(X_val, y_val) for clf in clfs]

fig, ax = plt.subplots()
ax.set_xlabel("alpha")
ax.set_ylabel("accuracy")
ax.set_title("Accuracy vs alpha for training and testing sets")
ax.plot(ccp_alphas, train_scores, marker='o', label="train",
        drawstyle="steps-post")
ax.plot(ccp_alphas, validation_scores, marker='o', label="validation",
        drawstyle="steps-post")
ax.legend()
plt.show()

# best: 0.0036
best = max(validation_scores)
print(best)
index = ccp_alphas[validation_scores.index(best)]
print(index)
final_model = DecisionTreeClassifier(ccp_alpha = index)
final_model = final_model.fit(x1_train, y1_train)
#final_model = clfs[index]
predictions = final_model.predict(x1_test)
acc = accuracy_score(y1_test, predictions)
matrix = confusion_matrix(y1_test, predictions)
print("accuracy: ", acc)
print(matrix)
print("Depth: ", final_model.get_depth())
'''
